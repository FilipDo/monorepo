from django.shortcuts import render
from .models import Book
def home(request):
    b = Book.objects.all()
    data = {
        "top_Test": b,
    }
    return render(request,"Test/Test.html", context=data)

def detail(request, book_id):
    book = Book.objects.get(pk=book_id)
    data = {
        "book": book,
    }
    return render(request, "Test/detail.html", context=data)

# Create your views here.
