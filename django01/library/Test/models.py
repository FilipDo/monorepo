from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=255, blank=False, null=False)
    year = models.IntegerField(blank=False, null=False)
    pages = models.IntegerField(blank=True, null=False, default=0)

    def __str__(self):
        return "{} ({})".format(self.title, self.year)
class Author(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    yearbd = models.IntegerField(blank=False, null=False)
    
    
    def __str__(self):
        return "{} ({})".format(self.name, self.yearbd)